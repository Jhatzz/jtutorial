package com.elavon.jtutorial.exer2;
// created by Jennifer Aguilar

public class ColoredTV extends TV{
	
	protected int brightness;
	protected int contrast;
	protected int picture;
	
	ColoredTV(String brand, String model) {
		super(brand, model);
		brightness = 50;
		contrast = 50;
		picture = 50;
	}
	
	public void brightnessUp() {
		brightness++;
	}
	
	public void brightnessDown() {
		brightness--;
	}
	
	public void contrastUp() {
		contrast++;
	}
	
	public void contrastDown() {
		contrast--;
	}

	public void pictureUp() {
		picture++;
	}
	
	public void pictureDown() {
		picture--;
	}
	
	public void swtichToChannel(int channel) {
		super.channel = channel;
	}
	
	public void mute() {
		super.volume = 0;
	}
	
	public String toString() {
		return(brand + " " + model + " [on:" + powerOn + ", channel:" + channel +  ", volume:" + volume + "] [b:" + brightness + ", c:" + contrast + ", p:" + picture + "]");
	}
	
	public static void main(String []args) {
		TV bnwTV, sonyTV;
		bnwTV = new TV("Admiral", "A1");
		sonyTV = new ColoredTV("SONY", "S1");
		System.out.println(bnwTV.toString());
		System.out.println(sonyTV.toString());
		//sonyTV.brightnessUp(); 
		//error since the method brightnessUp() is undefined for the type TV
        //    and not declared as ColoredTV
		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
		sharpTV.mute();
		
		for(int i = 0; i < 100; i++ ) {
			sharpTV.brightnessUp();
		}
		
		sharpTV.brightnessDown();
		
		for(int i = 0; i < 50; i++ ) {
			sharpTV.contrastUp();
		}
		
		sharpTV.contrastDown();
		
		for(int i = 0; i < 10; i++ ) {
			sharpTV.pictureUp();
		}
		
		sharpTV.pictureDown();
		
		System.out.println(sharpTV.toString());
	}
}
