package com.elavon.jtutorial.exer2;

public interface NetworkConnection {
	public NetworkConnection connect (String name);
	public boolean connectionStatus ();

}
