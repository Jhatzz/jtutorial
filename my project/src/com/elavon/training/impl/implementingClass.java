package com.elavon.training.impl;

import com.elavon.training.intf.AwesomeCalculator;

public class implementingClass implements AwesomeCalculator {

	@Override
	public int getSum(int augend, int addend) {
		// TODO Auto-generated method stub
		return (augend + addend);
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		// TODO Auto-generated method stub
		return (minuend - subtrahend);
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		// TODO Auto-generated method stub
		return (multiplicand * multiplier);
	}

	@Override
	public String getQuotientAndRemainder(int dividend, int divisor) {
		// TODO Auto-generated method stub
			
		return ("Quotient : " + dividend / divisor + " Remainder " + dividend % divisor);
	}

	@Override
	public double toCelsius(int fahrenheit) {
		// TODO Auto-generated method stub
		return ((fahrenheit - 32.00)*5/9);
	}

	@Override
	public double toFahrenheit(int celsius) {
		// TODO Auto-generated method stub
		return (celsius * 9/5 + 32.00);
	}

	@Override
	public double toKilogram(double lbs) {
		// TODO Auto-generated method stub
		return (lbs / 2.20462262185);
	}

	@Override
	public double toPound(double kg) {
		// TODO Auto-generated method stub
		return (kg * 2.20462262185);
	}

	@Override
	public boolean isPalindrome(String str) {
		// TODO Auto-generated method stub
		String lowstr = str.toLowerCase();
		int n = lowstr.length();
		  for (int i = 0; i < (n/2); ++i) {
		     if (lowstr.charAt(i) != lowstr.charAt(n - i - 1)) {
		         return false;
		     }
		  }

		  return true;
	}	
	
}
