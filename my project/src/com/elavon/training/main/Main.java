package com.elavon.training.main;

import com.elavon.training.impl.implementingClass;

public class Main {

	public static void main(String[] args) {
		
		implementingClass trial = new implementingClass ();
		System.out.println("6 + 11 = " + trial.getSum(6, 11));
		System.out.println("100 - 99 = " + trial.getDifference(100, 99));
		System.out.println("11 * 9 = " + trial.getProduct(11, 9));
		System.out.println("7 / 5 = " + trial.getQuotientAndRemainder(7, 5));
		System.out.println("100 Farenheit is equal to " + trial.toCelsius(100) + " Celcius");
		System.out.println("37 Celcius is equal to " + trial.toFahrenheit(37) + " Farenheit");
		System.out.println("10 Kgs is equal to " + trial.toPound(10) + " lbs");
		System.out.println("100 Lbs is equal to " + trial.toKilogram(100) + " kgs");
		System.out.println("Nasabayabasan is a palindrome?" + trial.isPalindrome("Nasabayabasan"));
		System.out.println("Jennifer is a palindrome?" + trial.isPalindrome("Jennifer"));
	}
}
