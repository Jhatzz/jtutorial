package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		
		TwelveDaysOfChristmas.printLyrics();
	}

	public static void printLyrics() {
		
		int number = 12;
		
		for(int i=1; i<=number; i++) {
			
			System.out.print("On the ");
		
			switch(i) {
			case 1: System.out.print("1st"); break;
			case 2: System.out.print("2nd"); break;
			case 3: System.out.print("3rd"); break;
			default: System.out.print(i+"th");
			}
		 		 
			System.out.println(" day of Christmas my true love gave to me");

		 	switch(i) {
		 	case 12: System.out.println("Twelve drummers drumming");
		 	case 11: System.out.println("Eleven pipers piping");
		 	case 10: System.out.println("Ten lords a-leaping");
		 	case 9: System.out.println("Nine ladies dancing");
		 	case 8: System.out.println("Eight maids a-milking");
		 	case 7: System.out.println("Seven swans a-swimming");
		 	case 6: System.out.println("Six geese a-laying");
		 	case 5: System.out.println("Five gold rings");
		 	case 4: System.out.println("Four colly birds");
		 	case 3: System.out.println("Three french hens");
		 	case 2: System.out.println("Two turtle doves and");
		 	case 1: System.out.println("A partridge in a pear tree.");
		 	}
		 	System.out.println();
		 	System.out.println();
		}
	}
}